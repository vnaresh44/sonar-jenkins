package com.engineer;

import static org.junit.Assert.*;
import  org.junit.Test;
public class FactorialTest {
    Factorial obj1 = new Factorial();
    
    

    @Test
    public void factorialOfFive(){
        assertEquals(120,obj1.factorial(5));
    }
    
    @Test
    
    public void factorialofFour() {
    	assertEquals(24,obj1.factorial(4));
    }
    
    @Test
    public void factorialofZero() {
    	assertEquals(1,obj1.factorial(0));
    }

}
